### DEV
* Run `npm webpack:dev` for build asset in development mode
* Run `npm start` for start server in development mode

### PROD
* Run `npm build` for build asset in production mode and compile `src` to `.build` directory
* Start server from `.build/app.js`
