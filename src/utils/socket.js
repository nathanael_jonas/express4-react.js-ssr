const io = require('socket.io');

module.exports = (http) => {
  const socket = io(http);
  socket.on('connection', (client) => {
    logger.socket.info('a user connected');
    client.on('message', (user, data, cb) => {
      logger.socket.silly(data);
      cb(data);
      client.broadcast.emit('message', user, data);
    });
  });
  return socket;
};
