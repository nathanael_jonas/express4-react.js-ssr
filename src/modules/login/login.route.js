import express from 'express';
import controllers from './login.controller';
const routes = express.Router(); // eslint-disable-line new-cap

routes.route('/')
    .get(controllers.getLoginView)
    .post(controllers.doLogin);

module.exports = routes;

