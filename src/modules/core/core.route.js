const express = require('express');
const routes = express.Router(); // eslint-disable-line new-cap
const controllers = require('./core.controller');

routes.get('/', controllers.getDetail);

module.exports = routes;

