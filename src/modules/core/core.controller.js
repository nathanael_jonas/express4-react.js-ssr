// @flow
import type {
  $Request,
  $Response,
  NextFunction,
} from 'express';
const path = require('path');

module.exports = {
  getDetail: (req: $Request, res: $Response, next: NextFunction) => {
    const file: string = path.resolve('./src/views/index.html');
    res.sendFile(file);
  },
};
