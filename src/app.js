require('dotenv').config();
require('./utils/logger');
const {PORT, NODE_ENV} = process.env;
const express = require('express');
const app = express();
const http = require('http').Server(app); // eslint-disable-line
const socket = require('./utils/socket')(http);

http.listen(PORT, () => {
  logger.server.info(`running on port ${PORT} env:${NODE_ENV}`);
});
if (NODE_ENV !== 'production') {
  const morgan = require('morgan');
  app.use(morgan('combined'));
}
app.set('view engine', 'ejs');
app.use('/public', express.static('public'));
app.use((req, res, next) => {
  res.socketio = socket;
  next();
});
app.use('/', require('./modules/core/core.route'));
app.use('/login', require('./modules/login/login.route'));
app.use((req, res, next) => {
  next({
    status: 404,
  });
});
app.use((payload, req, res, next) => {
  if (payload.success === true) {
    res.json(payload);
  } else if (payload.message) {
    res.status(payload.status || 400).json({
      succes: false,
      code: payload.code,
      message: payload.message,
      detail: NODE_ENV !== 'production' ? payload.stack : undefined,
    });
  } else {
    res.sendStatus(payload.status);
  }
});
